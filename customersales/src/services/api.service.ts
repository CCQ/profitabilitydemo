import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ClientData } from 'src/models/clientdata';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  private SERVER_URL = "http://localhost:3000/sales";

  constructor(private httpClient: HttpClient) { }

  public get(): Observable<ClientData[]> {
    return this.httpClient.get<ClientData[]>(this.SERVER_URL);
  }
}
