export class ClientData {
    public Client: string;
    public UnitsSold: number;
    public Income: number;
    public Cost: number;
}