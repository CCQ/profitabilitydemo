import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../services/api.service';
import { ClientData } from 'src/models/clientdata';

@Component({
  selector: 'app-sales',
  templateUrl: './sales.component.html',
  styleUrls: ['./sales.component.scss']
})
export class SalesComponent implements OnInit {
  clients = [];
  constructor(private apiService: ApiService) { }

  ngOnInit() {
    this.apiService.get().subscribe((response: ClientData[]) => {
      this.clients = response;
    })
  }

}
