# The Scenario #

Imagine you have been tasked by a client to create a basic reporting application that will allow them to view profitability and sales via  web portal.

You've been asked to create a frontend for a basic reporting app that keeps track of sales vs income vs cost to give an idea of which clients are the most profitable and most expensive.

The backend has been designed as a RESTful api which gives you all the data you need.

The back-end server that provides the public API can be hosted locally using the following commands:

```shell
$ npm install -g json-server

$ json-server --watch db.json
```

## 1. Designs have been supplied, but some artistic liberty is available! Show off what you can build!


## 2. What sort of user experience improvements would you make?


## 3. What sort of unit tests would you want for this app?


# The Results #

This is a mix of practical (this is the sort of work you might be asked to do) as well as creative.

There's no right way or wrong way to do this.

Use whatever js/css frameworks/tools/Google-fu you'd normally use.

We're looking to see how you approach the task, what tools you currently like to use and how you use them.